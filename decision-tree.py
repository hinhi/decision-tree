from graphviz import Digraph


class NameAlias:

    def __init__(self, *aliases):
        self.main = aliases[0]
        self.aliases = set(aliases)

    def __eq__(self, othr):
        if isinstance(othr, NameAlias):
            return self.main == othr.main
        return othr in self.aliases

    def __hash__(self):
        return hash(self.main)

    def get(self):
        return self.main


class Decision:
    
    def __init__(self, id, name, value, op, fomatter=None):
        self.id = str(id)
        self.name = name
        self.value = value
        if fomatter is None:
            fomatter = lambda s:s
        self.fomatter = fomatter
        if op == '以上':
            self.op = self.value.__le__
            self.op_str = ['以上', '未満']
        elif op == '以下':
            self.op = self.value.__ge__
            self.op_str = ['以下', '大きい']
        elif op == '大きい':
            self.op = self.value.__lt__
            self.op_str = ['大きい', '以下']
        elif op == '未満':
            self.op = self.value.__gt__
            self.op_str = ['未満', '以上']
        self.left = None
        self.right = None

    def add(self, child):
        if self.left is None:
            self.left = child
        elif self.right is None:
            self.right = child
        else:
            raise ValueError('Too many children')

    def do_eval(self, case):
        if self.op(case[self.name]):
            return self.left.do_eval(case)
        else:
            return self.right.do_eval(case)

    def get_str(self):
        return '%s\n%s' % (self.name, self.fomatter(self.value))

    def get_edge(self):
        return self.op_str

class Leaf:

    def __init__(self, id, value):
        self.id = str(id)
        self.value = value

    def do_eval(self, case):
        return self.value == case['間取り']

    def get_str(self):
        return self.value

def find_name(aliases, name):
    for alias in aliases:
        if alias == name:
            return alias

komoku = {
    NameAlias('賃料', '家賃', 'c', 'y'): '{:,d}円'.format,
    NameAlias('駅徒歩', '駅', '徒歩', 'e', 't'): '{:d}分'.format,
    NameAlias('占有面積', '面積', 's', 'm'): '{:.2f}㎡'.format,
}

madori = [
    NameAlias('1R', '1r', 'r'),
    NameAlias('1DK', '1dk', 'dk'),
    NameAlias('2LDK', '2ldk', '2'),
]

ops = [
    NameAlias('以上', '<='),
    NameAlias('以下', '>='),
    NameAlias('大きい', '<'),
    NameAlias('未満', '>'),
]



class Tree(object):

    def __init__(self, lis):
        self.node_count = 0
        self.root = self.parse(iter(lis))
        self.g = Digraph(format='png')

    def make_graph(self):
        lis = [self.root]
        s = set([])
        i = 0
        while i < len(lis):
            root = lis[i]
            self.g.attr('node', shape='box')
            self.g.node(root.id, root.get_str())
            


    def parse_komoku(self, it, x):
        name = find_name(komoku, x)
        if name is None:
            return
        val = int(next(it))
        x = next(it)
        op = find_name(ops, x)
        if op is None:
            raise ValueError('「%s」は分岐条件として不適切です'%x)
        self.node_count = 1
        return Decision(self.node_count, name.get(), val, op.get(), komoku[name])

    def parse_madori(self, it, x):
        name = find_name(madori, x)
        if name is None:
            return
        self.node_count = 1
        return Leaf(self.node_count, name.get())

    def parse(self, it, root=None):
        if root is None:
            x = next(it)
            root = self.parse_komoku(it, x)
            if root is None:
                raise ValueError('「%s」は項目名として不適切です'%x)
        for x in it:
            node = self.parse_komoku(it, x)
            if node is not None:
                root.add(node)
                self.parse(it, node)
                continue
            node = self.parse_madori(it, x)
            if node is not None:
                root.add(node)
                continue
            raise ValueError('木構造が適切ではありません')
        return root

if __name__ == '__main__':
    tree = Tree('面積 20 < 1R 賃料 70000 >= 2 1DK'.split())

